import React from 'react';
import {Form, Button} from 'react-bootstrap';
import './App.css';




class App extends  React.Component{
  constructor(props){
    super(props);
        this.state = {
          num1: "",
          num2: "",
          result: "",
          option:"+"
        }
        
  } 



  handleClick = () => {
   switch(this.state.option){
     case("+"):
     this.setState([this.state.result=this.state.num1 + this.state.num2])
     break;
     case("-"):
     this.setState([this.state.result=this.state.num1 - this.state.num2])
     break;
     case("*"):
     this.setState([this.state.result=this.state.num1 * this.state.num2])
     break;
     case("/"):
     this.setState([this.state.result=this.state.num1 / this.state.num2])
     break;
     default:
       break
   }
   
    
     
  }

  handleOptionChange = (e) =>{
    this.setState({option:e.currentTarget.value});
  }

  handleNum1Change = (e) =>{
    this.setState({num1: +e.currentTarget.value});
  }

  handleNum2Change = (e) =>{
    this.setState({num2: +e.currentTarget.value});
  }

  

  render(){
    return (
      <Form>  
          <Form.Group>
             <Form.Control value={this.state.num1} onChange={this.handleNum1Change} name="num1" placeholder="Введите число"/>
          </Form.Group>
          <Form.Group>
              <Form.Control value={this.state.option} onChange={this.handleOptionChange} as="select">
                      <option>+</option>
                      <option>-</option>
                      <option>*</option>
                      <option>/</option>
              </Form.Control>
          </Form.Group>
          <Form.Group >
               <Form.Control value={this.state.num2} onChange={this.handleNum2Change} name="num2" placeholder="Введите число"/>
          </Form.Group>
           <Form.Group>
                <Form.Control value={this.state.result} name="result" placeholder="результат"/>
           </Form.Group>
               <Button onClick={this.handleClick} variant="danger">Рассчитать</Button>
      </Form>
  
      
      
    );
  }
}

export default App;
